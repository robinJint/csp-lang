#![allow(unused_imports)]

use std::fmt::Write;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Read, Write as _};
use std::path::Path;
use std::process::{Command, Stdio};
use std::sync::Arc;

use walkdir::WalkDir;

fn test_eq(output: &[u8], path: &Path) -> Result<Result<(), String>, std::io::Error> {
    let mut buffer = Vec::new();
    if let Ok(mut f) = File::open(path) {
        f.read_to_end(&mut buffer)?;
    }

    let output_string = String::from_utf8_lossy(output);
    let buffer_string = String::from_utf8_lossy(&buffer);
    let output_str = output_string.trim();
    let buffer_str = buffer_string.trim();

    if output_str != buffer_str {
        let mut output = String::new();
        writeln!(&mut output, "mismatch in {}", path.display()).unwrap();
        writeln!(&mut output, "--- csp output:").unwrap();
        writeln!(&mut output, "{}", output_str).unwrap();
        writeln!(&mut output, "--- required output:").unwrap();
        writeln!(&mut output, "{}", buffer_str).unwrap();
        writeln!(&mut output, "---").unwrap();

        Ok(Err(output))
    } else {
        Ok(Ok(()))
    }
}

fn args_from_file(file: &Path) -> Option<Vec<String>> {
    let file = File::open(file).unwrap();
    let mut reader = BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line).unwrap();

    if line.starts_with("#:") {
        Some(line[3..].trim().split(" ").map(ToOwned::to_owned).collect())
    } else {
        None
    }
}

fn test_ui() -> Result<(), Box<dyn std::error::Error>> {
    let walker = WalkDir::new("tests/")
        .into_iter()
        .filter_map(|item| {
            if let Err(e) = &item {
                eprintln!("error: {}", e);
            }
            item.ok()
        })
        .filter(|p| p.path().extension() == Some("csp".as_ref()));

    let mut n_success = 0;
    let mut n_failed = 0;
    let mut errors = Vec::new();

    for entry in walker {
        eprint!("running: {} ...", entry.path().display());
        io::stderr().flush().unwrap();

        let args = args_from_file(&entry.path()).unwrap_or_default();

        let output = Command::new("./target/release/csp")
            .arg(entry.path())
            .arg("--hide-stats")
            .args(args)
            .args(std::env::args().skip(1))
            .output()?;

        let mut pathbuf = entry.path().to_path_buf();

        pathbuf.set_extension("stdout");
        let test_stdout = test_eq(&output.stdout, &pathbuf)?;
        pathbuf.set_extension("stderr");
        let test_stderr = test_eq(&output.stderr, &pathbuf)?;

        if test_stdout.is_ok() && test_stderr.is_ok() {
            n_success += 1;
            eprintln!("success");
        } else {
            n_failed += 1;
            eprintln!("failed");
        }

        if let Err(e) = test_stdout {
            errors.push(e)
        }
        if let Err(e) = test_stderr {
            errors.push(e)
        }
    }

    for error in errors {
        eprintln!("{}", error);
    }

    eprintln!("");
    eprintln!("{} tests successfull", n_success);
    eprintln!("{} tests failed", n_failed);

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let status = Command::new("cargo")
        .arg("build")
        .arg("--release")
        .status()?;

    assert!(status.success());

    test_ui()?;

    Ok(())
}
