use crate::passes::lowering::Lowered;
use core::hash::Hash;
use csp_lib_rs::{self as solver, Opcode, OpcodeT, VarId};
use std::{borrow::Cow, collections::HashMap, fmt::Debug, ops::Range};

use super::lowering::{LoweredExpr, LoweredFunc};
use crate::{
    ast::{BuildinFunc, ConstraintDecl, Csp, DomainDecl, Expr, Name, Op, RangeDecl, Typed, Value},
    Error,
};
use solver::rangevec::RangeVec;

pub trait ToLowered {
    type Identifier: Clone + Eq + Hash + Debug;

    fn variables(&self) -> Vec<(Self::Identifier, Vec<(isize, isize)>)>;
    fn constraints(&self) -> Vec<Vec<OpcodeT<Self::Identifier>>>;
    fn solutions(&self) -> Option<usize>;
}

pub fn compile<L: ToLowered>(
    lowered: L,
) -> Result<(solver::Csp, HashMap<usize, L::Identifier>), Error> {
    // use new fresh counting, so that the counting in continious and starts at 0
    let mut state = State::new(lowered);
    state.compile();
    let lookup = state
        .mapping
        .iter()
        .map(|(k, v)| (v.as_usize(), k.clone()))
        .collect();

    Ok((state.program, lookup))
}

struct State<'a, L: ToLowered + 'a> {
    // csp: &'a Csp<N, Typed>,
    _lifetime: std::marker::PhantomData<&'a ()>,
    lowered: L,
    mapping: HashMap<L::Identifier, solver::VarId>,
    program: solver::Csp,
}

impl<'a, L: ToLowered + 'a> State<'a, L> {
    pub fn new(lowered: L) -> Self {
        Self {
            _lifetime: std::marker::PhantomData::default(),
            lowered,
            mapping: HashMap::new(),
            program: solver::Csp::default(),
        }
    }

    fn register_var(&mut self, id: L::Identifier, domain: &[(isize, isize)]) {
        let domain = domain_to_rangeset(domain.iter().cloned());
        let new_var = self.program.add_var(domain);
        self.mapping.insert(id, new_var);
    }

    fn map_constraint(
        all_vars: &HashMap<L::Identifier, VarId>,
        constr: Vec<OpcodeT<L::Identifier>>,
    ) -> Vec<Opcode> {
        constr
            .iter()
            .map(|op| op.clone().map_t(|i| *all_vars.get(&i).unwrap()))
            .collect()
    }

    fn compile(&mut self) {
        self.program.number_of_solutions = self.lowered.solutions();

        let all_vars: HashMap<_, _> = self
            .lowered
            .variables()
            .into_iter()
            .map(|(i, domain)| {
                (
                    i,
                    self.program.add_var(
                        domain
                            .iter()
                            .map(|(l, r)| *l..r.saturating_add(1))
                            .collect::<RangeVec<_>>(),
                    ),
                )
            })
            .collect();

        for constr in self.lowered.constraints() {
            self.program
                .add_constraint(Self::map_constraint(&all_vars, constr))
        }

        self.mapping = all_vars;
    }
}

pub fn eval<N, C>(
    expr: &Expr<N, C>,
    mut lookup: impl FnMut(&Name<N, C>) -> isize + Copy,
) -> Vec<isize> {
    fn single(x: isize) -> Range<isize> {
        x..x + 1
    }
    match expr {
        Expr::Const(v) => vec![value_to_int(v)],
        Expr::Call(f, args) => match **f {
            BuildinFunc::Abs => eval(&args[0], lookup).iter().map(|val| val.abs()).collect(),
        },
        Expr::BinOp(op, l, r) => {
            let f: fn(isize, isize) -> Range<isize> = match **op {
                Op::Times => |l, r| single(l * r),
                Op::Div => |l, r| single(l / r),
                Op::Mod => |l, r| single(l % r),
                Op::Plus => |l, r| single(l + r),
                Op::Min => |l, r| single(l - r),
                Op::EQ => |l, r| single((l == r) as isize),
                Op::NEQ => |l, r| single((l != r) as isize),
                Op::GT => |l, r| single((l > r) as isize),
                Op::GE => |l, r| single((l >= r) as isize),
                Op::LT => |l, r| single((l < r) as isize),
                Op::LE => |l, r| single((l <= r) as isize),
                Op::BOr => |l, r| single(l | r),
                Op::Xor => |l, r| single(l ^ r),
                Op::Range => |l, r| l..r + 1,
            };
            eval(l, lookup)
                .into_iter()
                .zip(eval(r, lookup))
                .map(|(l, r)| f(l, r))
                .flatten()
                .collect()
        }
        Expr::Var(v) => vec![lookup(&**v)],
        Expr::Index(_, _) => panic!("internal error: no index in compile"),
    }
}

pub fn eval_one<N, C>(expr: &Expr<N, C>, lookup: impl FnMut(&Name<N, C>) -> isize + Copy) -> isize {
    let res = eval(expr, lookup);
    assert_eq!(res.len(), 1);
    res[0]
}

fn domain_to_rangeset<'a>(
    domain: impl Iterator<Item = (isize, isize)> + 'a,
) -> solver::rangevec::RangeVec<isize> {
    domain.map(|(l, r)| l..r + 1).collect()
}

fn rangedecl_to_range<C>(r: &RangeDecl<C>) -> Range<isize> {
    value_to_int(&r.start)..value_to_int(&r.end) + 1
}

pub fn value_to_int(value: &Value) -> isize {
    match value {
        Value::Bool(b) => *b as isize,
        Value::Number(n) => *n,
    }
}
