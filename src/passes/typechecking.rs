use crate::{
    ast::{ConstraintDecl, Csp, Expr, Loc, Metadata, Module, Name, Named, Op, Type, Typed, Value},
    error::error_to_diagnostic,
    Error,
};
use core::hash::Hash;
use std::collections::HashMap;

pub fn typecheck<N: Eq + Hash + AsRef<str>>(
    mut module: Module<N, Named>,
    errors: &mut Vec<Error>,
) -> Module<N, Typed> {
    let mut state = State {
        errors,
        variables: HashMap::new(),
    };
    state.collect(&mut module);
    state.typecheck(&mut module);
    module.change_constraint()
}

struct State<'a> {
    variables: HashMap<usize, (Loc, Type)>,
    errors: &'a mut Vec<Error>,
}

impl<'a> State<'a> {
    fn check_domains<N: Hash + Eq>(&mut self, csp: &Csp<N, Named>) {
        for vars in &csp.body.variables {
            for var in &vars.names {
                let id = var.get_id();
                if csp.try_find_domain(id).is_none() {
                    self.errors.push(Error::MissingDomain(var.loc))
                }
            }
        }
    }

    fn collect<N: Hash + Eq>(&mut self, module: &mut Module<N, Named>) {
        for csp in &mut module.csps {
            self.collect_csp(csp)
        }
    }

    fn collect_csp<N>(&mut self, csp: &mut Csp<N, Named>) {
        for input in &mut csp.input {
            let ty = input.ty.clone();
            for name in &mut input.names {
                self.collect_type(&ty, name)
            }
        }

        for output in &mut csp.output {
            let ty = output.ty.clone();
            for name in &mut output.names {
                self.collect_type(&ty, name)
            }
        }

        for vardecl in &mut csp.body.variables {
            let ty = vardecl.ty.clone();
            for name in &mut vardecl.names {
                self.collect_type(&ty, name)
            }
        }
    }

    fn collect_type<N>(&mut self, ty: &Type, name: &mut Metadata<Name<N, Named>, Named>) {
        self.variables.insert(name.get_id(), (name.loc, ty.clone()));
    }

    fn typecheck<N: Hash + Eq + AsRef<str>>(&mut self, module: &mut Module<N, Named>) {
        for csp in &mut module.csps {
            self.check_domains(csp);

            for constr in &mut csp.body.constraints {
                self.typecheck_constraint(constr)
            }
        }
    }

    fn typecheck_constraint<N: AsRef<str>>(&mut self, constraint: &mut ConstraintDecl<N, Named>) {
        match constraint {
            ConstraintDecl::Solve(csp, args) => {
                // TODO check that its equal to the args of the solve
                for arg in args {
                    self.typecheck_expr(arg, false);
                }
            }
            ConstraintDecl::Expr(expr) => {
                let ty = self.typecheck_expr(expr, false);
                if ty != Type::Bool {
                    self.errors.push(Error::TopLevelTypeError(expr.loc, ty))
                }
            }
            ConstraintDecl::Forall { name, range, body } => {
                let types: Vec<_> = range
                    .iter_mut()
                    .map(|range| (range.loc, self.typecheck_expr(range, true)))
                    .collect();
                for types in types.windows(2) {
                    let l = &types[0];
                    let r = &types[1];
                    if l.1 != r.1 {
                        self.errors
                            .push(Error::TypeError(l.0, l.1.clone(), r.0, r.1.clone()));
                    }
                }

                let previous = self
                    .variables
                    .insert(name.get_id(), (name.loc, Type::Integer));

                for sub_constraint in body {
                    self.typecheck_constraint(sub_constraint);
                }

                if let Some(prev) = previous {
                    self.variables.insert(name.get_id(), prev);
                } else {
                    self.variables.remove(&name.get_id());
                }
            }
        }
    }

    fn typecheck_expr<N: AsRef<str>>(&mut self, expr: &mut Expr<N, Named>, is_const: bool) -> Type {
        match expr {
            Expr::Const(c) => match **c {
                Value::Number(_) => Type::Integer,
                Value::Bool(_) => Type::Bool,
            },
            Expr::Var(name) => self.variables.get(&name.get_id()).unwrap().1.clone(),
            Expr::BinOp(op, l, r) => {
                let l_ty = self.typecheck_expr(&mut *l, is_const);
                let r_ty = self.typecheck_expr(&mut *r, is_const);

                if l_ty.bottom_type() != r_ty.bottom_type() {
                    self.errors
                        .push(Error::TypeError(l.loc, l_ty.clone(), r.loc, r_ty));
                }

                match **op {
                    Op::EQ | Op::NEQ | Op::LT | Op::GT | Op::LE | Op::GE => Type::Bool,
                    Op::Plus | Op::Min | Op::Times | Op::Div | Op::BOr | Op::Xor | Op::Mod => {
                        Type::Integer
                    }
                    Op::Range => {
                        if !is_const {
                            self.errors.push(Error::RangeInNonConst(op.loc));
                        }

                        Type::Array {
                            subtype: Box::new(l_ty),
                            len: 0,
                        }
                    }
                }
            }
            Expr::Call(f, args) => {
                for sub_expr in args {
                    self.typecheck_expr(sub_expr, is_const);
                }
                match **f {
                    crate::ast::BuildinFunc::Abs => Type::Integer,
                }
            }
            Expr::Index(base, args) => {
                for arg in args {
                    for expr in arg {
                        self.typecheck_expr(expr, true);
                    }
                }

                // not completely correct as the depth might change, but good enough
                self.typecheck_expr(base, is_const)
            }
        }
    }
}
