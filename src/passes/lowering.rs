use super::compiling::eval_one;
use crate::passes::compiling::eval;
use crate::passes::compiling::value_to_int;
use crate::{
    ast::{
        BuildinFunc, ConstraintDecl, Csp, CspBody, Expr, IsNamed, Loc, Metadata, Module, Name, Op,
        RangeDecl, Solutions, Type, Typed,
    },
    passes::compiling::ToLowered,
    Error,
};
use core::hash::Hash;
use csp_lib_rs::OpcodeT;
use std::{collections::HashMap, convert::TryFrom, iter::Step};

/// lower the ast
///
/// this does two things
/// - create one 'big' csp by inlining all solve calls
/// - use only 'simple' int variables
pub fn lower<N: AsRef<str> + Hash + Eq + Clone + std::fmt::Debug>(
    module: &Module<N, Typed>,
) -> Result<Lowered, Error> {
    let lowered: HashMap<_, _> = module
        .csps
        .iter()
        .filter_map(|csp| -> Option<Result<_, _>> {
            let mut lower_state = LowerState::default();

            match lower_state.lower(csp) {
                Ok(lower) => Some(Ok((csp.name.as_ref()?.get_id(), lower))),
                Err(e) => Some(Err(e)),
            }
        })
        .collect::<Result<HashMap<_, _>, Error>>()?;

    let main: &Csp<N, Typed> = module.get_main_csp().unwrap();
    let mut lower_state = LowerState::default();

    let mut lower_main = lower_state.lower(main)?;

    let mut combine_state = CombineState::new(lowered);
    while lower_main.has_sub_solves() {
        combine_state.inline(&mut lower_main)?;
    }

    Ok(lower_main)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Identifier(usize, usize);

impl Identifier {
    pub fn blank_new(n: usize) -> Self {
        Self(0, n)
    }

    pub fn map_generation(&self, gen: usize) -> Self {
        assert_ne!(gen, 0);

        Self(gen, self.1)
    }
}

struct CombineState {
    csps: HashMap<usize, Lowered>,
}

impl CombineState {
    fn new(csps: HashMap<usize, Lowered>) -> Self {
        Self { csps }
    }

    fn inline(&mut self, main: &mut Lowered) -> Result<(), Error> {
        let sub_solves = std::mem::take(&mut main.solves);
        for sub_solve in sub_solves {
            let sub_csp = self.csps.get(&sub_solve.sub_solve).expect("missing csp");

            main.inline(sub_csp.clone(), sub_solve.args.iter().cloned());
        }

        Ok(())
    }
}

#[derive(Default)]
struct LowerState {
    /// mapping from (varid, [index]) to a new varid
    ///
    /// for example if we have a var x: [integer; 4]
    /// then mapping.get((x.varid, vec![1])) would return the new id for the
    /// second element in the x array
    ///
    mapping: HashMap<(usize, Vec<usize>), Identifier>,
    /// a mapping from an expr's identifier (usize) to variable (that potentially is an array (VarMapping))
    mapping2: HashMap<usize, VarMapping>,
    type_mapping: HashMap<Identifier, Type>,
    domains: HashMap<Identifier, Vec<(isize, isize)>>,

    fresh_id: usize,
}

#[derive(Debug, Clone)]
pub struct Lowered {
    pub highest_gen: usize,
    pub variables: Vec<(Identifier, Vec<(isize, isize)>)>,
    pub constraints: Vec<LoweredExpr>,
    pub inputs: Vec<Identifier>,
    pub solves: Vec<LoweredSolve>,
    pub lookup: HashMap<Identifier, (usize, Vec<usize>)>,
    pub solutions: Option<usize>,
}

impl ToLowered for &Lowered {
    type Identifier = Identifier;

    fn variables(&self) -> Vec<(Self::Identifier, Vec<(isize, isize)>)> {
        let mut vec = self.variables.clone();
        vec.sort_unstable_by_key(|(i, _)| (i.0, i.1));
        vec
        // let mut vec: Vec<_> = self.variables.keys().copied().collect();
        // vec.sort_unstable_by_key(|i| (i.0, i.1));
        // vec
    }

    fn constraints(&self) -> Vec<Vec<OpcodeT<Self::Identifier>>> {
        self.constraints
            .iter()
            .map(|constr| {
                let mut program = Vec::new();
                self.compile_expr(constr, &mut program);
                program
            })
            .collect()
    }

    // fn domain(&self, ident: &Self::Identifier) -> &[(isize, isize)] {
    //     &self.variables[ident]
    // }

    fn solutions(&self) -> Option<usize> {
        self.solutions
    }
}

impl Lowered {
    fn has_sub_solves(&self) -> bool {
        !self.solves.is_empty()
    }

    fn map_identifier(&mut self, f: &mut impl FnMut(&mut Identifier)) {
        for (ref mut var, _) in self.variables.iter_mut() {
            f(var)
        }

        for constr in self.constraints.iter_mut() {
            constr.map_identifier(f)
        }

        for input in self.inputs.iter_mut() {
            f(input)
        }

        for solve in self.solves.iter_mut() {
            solve.map_identifier(f)
        }

        self.lookup = std::mem::take(&mut self.lookup)
            .into_iter()
            .map(|(i, v)| {
                let mut new_i = i;
                f(&mut new_i);
                (new_i, v)
            })
            .collect();
    }

    fn inline(&mut self, mut other: Self, args: impl Iterator<Item = LoweredExpr>) {
        assert_eq!(other.highest_gen, 0);

        let input_mapping: HashMap<_, _> = other.inputs.iter().copied().zip(args).collect();

        self.highest_gen += 1;
        let new_gen = self.highest_gen;

        for constr in &mut other.constraints {
            constr.map_identifier_expr(&mut |i| {
                input_mapping
                    .get(&i)
                    .cloned()
                    .unwrap_or(LoweredExpr::Var(i))
            })
        }

        self.variables.extend(other.variables);
        self.constraints.extend(other.constraints);
        self.solves.extend(other.solves);
        std::mem::swap(&mut self.lookup, &mut other.lookup);
        self.lookup.extend(other.lookup);
    }

    fn compile_expr(&self, expr: &LoweredExpr, program: &mut Vec<OpcodeT<Identifier>>) {
        match expr {
            LoweredExpr::Const(value) => {
                program.push(OpcodeT::Const(*value));
            }
            LoweredExpr::Var(var) => {
                program.push(OpcodeT::Var(*var));
            }
            LoweredExpr::Call(LoweredFunc::Binop(op), args) => {
                let (l, r) = match &**args {
                    [l, r] => (l, r),
                    _ => panic!(),
                };
                self.compile_expr(l, program);
                self.compile_expr(r, program);
                match op {
                    Op::EQ => program.push(OpcodeT::EQ),
                    Op::NEQ => program.push(OpcodeT::NEQ),
                    Op::LT => program.push(OpcodeT::LT),
                    Op::GT => program.push(OpcodeT::GT),
                    Op::LE => program.push(OpcodeT::LE),
                    Op::GE => program.push(OpcodeT::GE),
                    Op::Plus => program.push(OpcodeT::Add),
                    Op::Min => program.push(OpcodeT::Sub),
                    Op::BOr => program.push(OpcodeT::BOR),
                    Op::Xor => program.push(OpcodeT::XOR),
                    Op::Times => program.push(OpcodeT::Mul),
                    Op::Div => program.push(OpcodeT::Div),
                    Op::Mod => program.push(OpcodeT::Mod),
                    Op::Range => {
                        panic!("range ('..') operators should not be possible in lowering")
                    }
                }
            }
            LoweredExpr::Call(LoweredFunc::Func(BuildinFunc::Abs), args) => {
                self.compile_expr(&args[0], program);
                program.push(OpcodeT::Abs);
            }
            _ => panic!(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum LoweredFunc {
    Binop(Op),
    Func(BuildinFunc),
}

#[derive(Debug, Clone)]
pub enum VarMapping {
    Concrete(Identifier),
    Array(Vec<VarMapping>, usize),
    // Virtual(Vec<(usize, VarMapping)>),
}

impl VarMapping {
    fn index(&self, i: usize) -> &VarMapping {
        match self {
            VarMapping::Concrete(_) => panic!("index into concrete"),
            VarMapping::Array(vec, _) => &vec[i],
        }
    }

    fn max_index(&self) -> usize {
        match self {
            VarMapping::Concrete(_) => panic!("index into concrete"),
            VarMapping::Array(vec, _) => vec.len() - 1,
        }
    }

    fn index_owned(self, i: usize) -> VarMapping {
        match self {
            VarMapping::Concrete(_) => panic!("index into concrete"),
            VarMapping::Array(vec, _) => vec.into_iter().nth(i).unwrap(),
        }
    }

    fn index_many(&self, args: impl Iterator<Item = usize>) -> Self {
        match self {
            VarMapping::Concrete(_) => panic!("index many into concrete"),
            VarMapping::Array(vec, depth) => {
                let mut new = Vec::with_capacity(args.size_hint().0);
                for i in args {
                    new.push(vec[i].clone())
                }
                VarMapping::Array(new, *depth)
            }
        }
    }

    pub fn unwrap_single(&self) -> Identifier {
        match self {
            VarMapping::Concrete(v) => *v,
            _ => panic!("unwrapping varmapping"),
        }
    }

    fn get_values<'a>(&'a self) -> Box<dyn Iterator<Item = Identifier> + 'a> {
        match self {
            VarMapping::Concrete(var) => Box::new(std::iter::once(*var)),
            VarMapping::Array(vars, _) => Box::new(vars.iter().map(|vm| vm.get_values()).flatten()),
        }
    }

    fn depth(&self) -> usize {
        match self {
            VarMapping::Array(_, d) => *d,
            _ => 0,
        }
    }

    fn map_identifier(&mut self, f: &mut impl FnMut(&mut Identifier)) {
        match self {
            VarMapping::Concrete(ref mut i) => f(i),
            VarMapping::Array(elems, _) => elems.iter_mut().for_each(|e| e.map_identifier(f)),
        }
    }

    fn map_owned(self, depth_change: isize, f: impl FnMut(Self) -> Self) -> Self {
        match self {
            VarMapping::Array(vec, d) => {
                let res = vec.into_iter().map(f).collect();
                VarMapping::Array(res, (d as isize + depth_change) as usize)
            }
            VarMapping::Concrete(_) => panic!(),
        }
    }

    fn to_expr(&self) -> LoweredExpr {
        match self {
            VarMapping::Concrete(i) => LoweredExpr::Var(*i),
            VarMapping::Array(xs, d) => {
                LoweredExpr::Array(xs.iter().map(|vm| vm.to_expr()).collect(), *d)
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct LoweredSolve {
    sub_solve: usize,
    args: Vec<LoweredExpr>,
}

impl LoweredSolve {
    fn map_identifier(&mut self, f: &mut impl FnMut(&mut Identifier)) {
        self.args.iter_mut().for_each(|e| e.map_identifier(f))
    }
}

#[derive(Debug, Clone)]
/// Expr without the ast details
///
/// currently has varmapping and array, they mostly have the same representation and could be unified
pub enum LoweredExpr {
    // Var(VarMapping),
    Var(Identifier),
    Const(isize),
    Array(Vec<LoweredExpr>, usize),
    Call(LoweredFunc, Vec<LoweredExpr>),
    Error,
}

impl LoweredExpr {
    fn depth(&self) -> usize {
        match self {
            // LoweredExpr::Var(v) => v.depth(),
            LoweredExpr::Array(_, d) => *d,
            _ => 0,
        }
    }

    fn lower(&self) -> Vec<Self> {
        match self {
            // LoweredExpr::Var(v) => match v {
            //     VarMapping::Array(v, d) => v.iter().map(|v| LoweredExpr::Var(v.clone())).collect(),
            //     _ => panic!(),
            // },
            LoweredExpr::Array(v, _) => v.clone(),
            _ => panic!(),
        }
    }

    fn lower_until(&self, n: usize) -> Vec<Self> {
        if self.depth() <= n {
            return vec![self.clone()];
        }

        self.lower()
            .into_iter()
            .map(|e| e.lower_until(n + 1))
            .flatten()
            .collect()
    }

    fn map_identifier(&mut self, f: &mut impl FnMut(&mut Identifier)) {
        match self {
            LoweredExpr::Var(i) => f(i),
            LoweredExpr::Const(_) => {}
            LoweredExpr::Array(elems, _) => elems.iter_mut().for_each(|e| e.map_identifier(f)),
            LoweredExpr::Call(_, args) => args.iter_mut().for_each(|e| e.map_identifier(f)),
            _ => {}
        }
    }

    fn map_identifier_expr(&mut self, f: &mut impl FnMut(Identifier) -> LoweredExpr) {
        match self {
            LoweredExpr::Var(i) => *self = f(*i),
            LoweredExpr::Const(_) => {}
            LoweredExpr::Array(xs, _) => xs.iter_mut().for_each(|e| e.map_identifier_expr(f)),
            LoweredExpr::Call(_, args) => args.iter_mut().for_each(|e| e.map_identifier_expr(f)),
            LoweredExpr::Error => {}
        }
    }

    fn map(&self, f: impl FnMut(&Self) -> Self) -> Self {
        match self {
            LoweredExpr::Array(vec, d) => LoweredExpr::Array(vec.iter().map(f).collect(), *d),
            _ => panic!(),
        }
    }

    fn map_at_depth(&self, n: usize, f: &mut impl FnMut(&Self) -> Self) -> Self {
        if self.depth() < n {
            panic!()
        } else if self.depth() == n {
            f(self)
        } else {
            self.map(move |e| e.map_at_depth(n, f))
        }
    }
}

enum Value<T> {
    Array(Vec<ValueInner<T>>),
    Inner(ValueInner<T>),
}

impl<T: Step + Copy> Value<T> {
    fn iter<'a>(&'a self) -> Box<dyn Iterator<Item = T> + 'a> {
        match self {
            Value::Array(vec) => Box::new(vec.iter().map(|inner| inner.iter()).flatten()),
            Value::Inner(i) => Box::new(i.iter()),
        }
    }

    fn unwrap_single(&self) -> &T {
        match self {
            Value::Inner(ValueInner::Single(s)) => s,
            _ => panic!(),
        }
    }
}

enum ValueInner<T> {
    Single(T),
    Range(T, T),
}

impl<T: Step + Copy> ValueInner<T> {
    fn iter(&self) -> impl Iterator<Item = T> {
        match self {
            ValueInner::Single(t) => *t..Step::forward(*t, 1),
            ValueInner::Range(s, e) => *s..*e,
        }
    }

    fn unwrap_single(&self) -> &T {
        match self {
            ValueInner::Single(s) => s,
            _ => panic!(),
        }
    }
}

enum Value2<T> {
    One(T),
    More(Vec<T>),
}

/// A (partial) index into a variable
///
/// Index(10, [1,2,3]) would be Var(10)[1][2][3]
struct Index(usize, Vec<usize>);

fn eval_lower<N, C: IsNamed>(expr: &Expr<N, C>, vars: &HashMap<usize, isize>) -> ValueInner<isize> {
    match expr {
        Expr::Var(v) => (ValueInner::Single(*vars.get(&v.get_id()).unwrap())),
        Expr::Const(c) => (ValueInner::Single(value_to_int(c))),
        Expr::BinOp(op, l, r) => {
            let f: fn(isize, isize) -> ValueInner<isize> = match **op {
                Op::Times => |l, r| ValueInner::Single(l * r),
                Op::Div => |l, r| ValueInner::Single(l / r),
                Op::Mod => |l, r| ValueInner::Single(l % r),
                Op::Plus => |l, r| ValueInner::Single(l + r),
                Op::Min => |l, r| ValueInner::Single(l - r),
                Op::BOr => |l, r| ValueInner::Single(l | r),
                Op::Xor => |l, r| ValueInner::Single(l ^ r),
                Op::EQ => |l, r| ValueInner::Single((l == r) as isize),
                Op::NEQ => |l, r| ValueInner::Single((l != r) as isize),
                Op::LT => |l, r| ValueInner::Single((l < r) as isize),
                Op::GT => |l, r| ValueInner::Single((l > r) as isize),
                Op::LE => |l, r| ValueInner::Single((l <= r) as isize),
                Op::GE => |l, r| ValueInner::Single((l >= r) as isize),
                Op::Range => |l, r| ValueInner::Range(l, r),
            };
            let l = *eval_lower(l, vars).unwrap_single();
            let r = *eval_lower(r, vars).unwrap_single();
            f(l, r)
        }
        Expr::Call(func, args) => match **func {
            BuildinFunc::Abs => {
                let arg = &args[0];
                let value = eval_lower(arg, vars);
                ValueInner::Single(value.unwrap_single().abs())
            }
        },
        Expr::Index(_, _) => panic!("internal error: no indexing in lower"),
    }
}

impl LowerState {
    fn lower<N: std::fmt::Debug + AsRef<str>>(
        &mut self,
        csp: &Csp<N, Typed>,
    ) -> Result<Lowered, Error> {
        self.build_mapping(csp);
        self.build_domains(csp);

        let lookup = self.build_reverse();
        let mut errors = Vec::new();

        let (constraints, solves): (Vec<_>, Vec<_>) = csp
            .body
            .constraints
            .iter()
            .map(|e| self.lower_constraint(&**e, &mut HashMap::new(), &mut errors))
            .unzip();

        let constraints = constraints.into_iter().flatten().collect();

        let inputs = csp
            .input
            .iter()
            .map(|vardecl| vardecl.names.iter())
            .flatten()
            .map(|name| self.mapping2.get(&name.get_id()).unwrap().unwrap_single())
            .collect();

        if !errors.is_empty() {
            return Err(Error::Errors(errors));
        }

        Ok(Lowered {
            lookup,
            highest_gen: 0,
            variables: self.domains.iter().map(|(k, v)| (*k, v.clone())).collect(),
            constraints,
            inputs,
            solves: solves.into_iter().flatten().collect(),
            solutions: csp
                .body
                .solutions
                .as_ref()
                .map(|s| match **s {
                    Solutions::N(n) => Some(n),
                    Solutions::All => None,
                })
                .unwrap_or(Some(1)),
        })
    }

    fn fresh_num(&mut self) -> usize {
        let id = self.fresh_id;
        self.fresh_id += 1;
        id
    }

    fn fresh_blank_id(&mut self) -> Identifier {
        Identifier::blank_new(self.fresh_num())
    }

    fn build_reverse(&self) -> HashMap<Identifier, (usize, Vec<usize>)> {
        self.mapping.iter().map(|(k, v)| (*v, k.clone())).collect()
    }

    fn type_indexes<'a>(ty: &Type, buffer: &'a mut Vec<usize>, f: &mut impl FnMut(&Vec<usize>)) {
        match ty {
            Type::Array { subtype, len } => {
                for i in 0..*len {
                    buffer.push(i);
                    Self::type_indexes(subtype, buffer, f);
                    buffer.pop();
                }
            }
            _ => f(buffer),
        }
    }

    fn lower_constraint<N: std::fmt::Debug + AsRef<str>>(
        &self,
        constraint: &ConstraintDecl<N, Typed>,
        forall_mapping: &mut HashMap<usize, isize>,
        errors: &mut Vec<Error>,
    ) -> (Vec<LoweredExpr>, Vec<LoweredSolve>) {
        match constraint {
            ConstraintDecl::Expr(e) => {
                let lower = self.lower_expr(&**e, forall_mapping, errors);

                (self.flatten_expr(&lower), Vec::new())
            }
            ConstraintDecl::Solve(n, args) => {
                let args: Vec<_> = args
                    .iter()
                    .map(|arg| {
                        let expr = self.lower_expr(arg, forall_mapping, errors);
                        self.flatten_expr(&expr)
                    })
                    .flatten()
                    .collect();

                match n.name.as_ref() {
                    "alldiff" => {
                        let mut exprs = Vec::new();
                        for i in 0..args.len() - 1 {
                            for j in i + 1..args.len() {
                                exprs.push(LoweredExpr::Call(
                                    LoweredFunc::Binop(Op::NEQ),
                                    vec![args[i].clone(), args[j].clone()],
                                ))
                            }
                        }
                        return (exprs, Vec::new());
                    }
                    _ => {}
                }

                (
                    Vec::new(),
                    vec![LoweredSolve {
                        sub_solve: n.get_id(),
                        args,
                    }],
                )
            }
            ConstraintDecl::Forall { name, range, body } => {
                let mut new_constraints = Vec::new();
                let mut new_solves = Vec::new();
                let lookup = |name: &Name<N, Typed>| *forall_mapping.get(&name.get_id()).unwrap();
                let ranges: Vec<_> = range.iter().map(|r| eval(r, lookup)).collect();

                for value in ranges.into_iter().flatten() {
                    forall_mapping.insert(name.get_id(), value);
                    for sub_constraint in body {
                        let (sub_constraints, sub_solves) =
                            self.lower_constraint(sub_constraint, forall_mapping, errors);
                        new_constraints.extend(sub_constraints);
                        new_solves.extend(sub_solves);
                    }
                }
                forall_mapping.remove(&name.get_id());

                (new_constraints, new_solves)
            }
        }
    }

    fn flatten_expr(&self, expr: &LoweredExpr) -> Vec<LoweredExpr> {
        match expr {
            // LoweredExpr::Var(v) => v
            //     .get_values()
            //     .map(|v| LoweredExpr::Var(VarMapping::Concrete(v)))
            //     .collect(),
            LoweredExpr::Var(i) => vec![LoweredExpr::Var(*i)],
            LoweredExpr::Const(c) => vec![LoweredExpr::Const(*c)],
            LoweredExpr::Array(vec, _) => {
                vec.iter().map(|e| self.flatten_expr(e)).flatten().collect()
            }
            LoweredExpr::Call(LoweredFunc::Binop(op), args) => {
                let l = &args[0];
                let r = &args[1];

                self.flatten_expr(l)
                    .into_iter()
                    .zip(self.flatten_expr(r))
                    .map(|(l, r)| LoweredExpr::Call(LoweredFunc::Binop(*op), vec![l, r]))
                    .collect()
            }
            LoweredExpr::Call(LoweredFunc::Func(func), args) => {
                let arg = &args[0];
                self.flatten_expr(arg)
                    .into_iter()
                    .map(|arg| LoweredExpr::Call(LoweredFunc::Func(*func), vec![arg]))
                    .collect()
            }
            LoweredExpr::Error => vec![],
        }
    }

    /// Convert an Expr into a LoweredExpr
    /// - resolves all vars into explicit locations
    /// - makes all bulk operations explicit
    /// - lowers values to integer or arrays of integer
    ///
    /// This does not:
    /// - flatten the expr
    fn lower_expr<N>(
        &self,
        expr: &Expr<N, Typed>,
        forall_mapping: &mut HashMap<usize, isize>,
        errors: &mut Vec<Error>,
    ) -> LoweredExpr {
        match expr {
            Expr::Var(v) => {
                match forall_mapping.get(&v.get_id()) {
                    None => {}
                    Some(c) => return LoweredExpr::Const(*c),
                }

                let mapping = self.mapping2.get(&v.get_id()).unwrap();
                mapping.to_expr()
            }
            Expr::Const(c) => LoweredExpr::Const(value_to_int(c)),
            Expr::BinOp(op, l, r) => {
                let l = self.lower_expr(l, forall_mapping, errors);
                let r = self.lower_expr(r, forall_mapping, errors);

                // expand the most depthiest between l and r until they are equal
                // then actually do the binop

                if l.depth() == r.depth() {
                    LoweredExpr::Call(LoweredFunc::Binop(**op), vec![l, r])
                } else if l.depth() > r.depth() {
                    l.map_at_depth(r.depth(), &mut |l| {
                        LoweredExpr::Call(LoweredFunc::Binop(**op), vec![l.clone(), r.clone()])
                    })
                } else {
                    r.map_at_depth(l.depth(), &mut |r| {
                        LoweredExpr::Call(LoweredFunc::Binop(**op), vec![l.clone(), r.clone()])
                    })
                }
            }
            Expr::Call(f, args) => match **f {
                BuildinFunc::Abs => {
                    assert_eq!(args.len(), 1);
                    let arg = self.lower_expr(&args[0], forall_mapping, errors);
                    arg.map_at_depth(0, &mut |e| {
                        LoweredExpr::Call(LoweredFunc::Func(BuildinFunc::Abs), vec![e.clone()])
                    })
                }
            },
            Expr::Index(base, args) => match self.lower_index(base, args, forall_mapping) {
                Ok(v) => v.to_expr(),
                Err(e) => {
                    errors.push(e);
                    LoweredExpr::Error
                }
            },
        }
    }

    fn lower_index<N>(
        &self,
        base: &Expr<N, Typed>,
        args: &Vec<Vec<Metadata<Expr<N, Typed>, Typed>>>,
        forall_mapping: &mut HashMap<usize, isize>,
    ) -> Result<VarMapping, Error> {
        fn f(var: VarMapping, args: &[(Value<isize>, Loc)], errors: &mut Vec<Error>) -> VarMapping {
            match args {
                [] => var,
                [(x, x_loc), xs @ ..] => match x {
                    Value::Inner(ValueInner::Single(n)) => {
                        let index = if *n < 0 || *n > var.max_index() as isize {
                            errors.push(Error::IndexOutofbounds {
                                loc: *x_loc,
                                lower_bound: 0,
                                upper_bound: var.max_index() as isize,
                                index: *n,
                            });
                            return VarMapping::Array(vec![], 0);
                        } else {
                            *n as usize
                        };
                        let indexed = var.index(index);
                        f(indexed.clone(), &args[1..], errors)
                    }
                    Value::Inner(ValueInner::Range(l, r)) => {
                        let indexed = var.index_many(
                            usize::try_from(*l).unwrap()..=usize::try_from(*r).unwrap(),
                        );
                        indexed.map_owned(0, |var| f(var, &args[1..], errors))
                    }
                    Value::Array(vec) => {
                        let indexed = var.index_many(
                            vec.iter()
                                .map(|v| v.iter())
                                .flatten()
                                .map(|i| usize::try_from(i).unwrap()),
                        );
                        indexed.map_owned(0, |var| f(var, &args[1..], errors))
                    }
                },
            }
        }
        let var = match base {
            Expr::Var(v) => self.mapping2.get(&v.get_id()).unwrap().clone(),
            Expr::Index(b2, i2) => self.lower_index(b2, i2, forall_mapping)?,
            _ => panic!("index of not var"),
        };

        let index_args: Vec<_> = args
            .iter()
            .map(|ranges| {
                if ranges.is_empty() {
                    panic!()
                } else if ranges.len() == 1 {
                    (
                        Value::Inner(eval_lower(&ranges[0], forall_mapping)),
                        ranges[0].loc,
                    )
                } else {
                    let arr = ranges
                        .iter()
                        .map(|range| eval_lower(range, forall_mapping))
                        .collect();

                    (Value::Array(arr), ranges[0].loc)
                }
            })
            .collect();

        let mut errors = Vec::new();
        let result = f(var, &index_args, &mut errors);
        if !errors.is_empty() {
            Err(Error::Errors(errors))
        } else {
            Ok(result)
        }
    }

    fn build_domain_type(
        &mut self,
        ty: &Type,
        id: usize,
        index: &Vec<usize>,
        domain: &Vec<(isize, isize)>,
    ) {
        match ty {
            Type::Error => panic!("error type in lower"),
            Type::Integer | Type::Bool => {
                let new_id = self.mapping.get(&(id, index.clone())).unwrap();
                self.domains.insert(*new_id, domain.clone());
            }
            Type::Array { subtype, len } => {
                for i in 0..*len {
                    let mut new_index = index.clone();
                    new_index.push(i);
                    self.build_domain_type(subtype, id, &new_index, domain)
                }
            }
        }
    }

    fn build_domains<N>(&mut self, csp: &Csp<N, Typed>) {
        for domaindecl in &csp.body.domains {
            let domain = domaindecl
                .domain
                .iter()
                .map(|rd| (value_to_int(&*rd.start), value_to_int(&*rd.end)))
                .collect();

            for var in &domaindecl.vars {
                let ty = self
                    .type_mapping
                    .get(&Identifier::blank_new(var.get_id()))
                    .unwrap()
                    .clone();
                self.build_domain_type(&ty, var.get_id(), &Vec::new(), &domain)
            }
        }
    }

    fn build_type(&mut self, ty: &Type, var: usize, index: &Vec<usize>) -> VarMapping {
        match ty {
            Type::Error => panic!("error type in lower"),
            Type::Integer | Type::Bool => {
                let id = self.fresh_blank_id();
                self.mapping.insert((var, index.clone()), id);
                VarMapping::Concrete(id)
            }
            Type::Array { subtype, len } => {
                let res = (0..*len)
                    .map(|i| {
                        let mut new_index = index.clone();
                        new_index.push(i);
                        self.build_type(subtype, var, &new_index)
                    })
                    .collect();

                VarMapping::Array(res, ty.depth())
            }
        }
    }

    fn build_mapping<N>(&mut self, csp: &Csp<N, Typed>) {
        for vars in &csp.body.variables {
            let ty = &vars.ty;
            for var in &vars.names {
                self.type_mapping
                    .insert(Identifier::blank_new(var.get_id()), (*vars.ty).clone());
                let varmapping = self.build_type(ty, var.get_id(), &Vec::new());
                self.mapping2.insert(var.get_id(), varmapping);
            }
        }

        for input in &csp.input {
            let ty = &input.ty;
            for var in &input.names {
                self.type_mapping
                    .insert(Identifier::blank_new(var.get_id()), (*input.ty).clone());
                let varmapping = self.build_type(ty, var.get_id(), &Vec::new());
                self.mapping2.insert(var.get_id(), varmapping);
            }
        }
    }
}
