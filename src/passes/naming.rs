use codespan::{FileId, Span};
use core::hash::Hash;
use std::{
    collections::{hash_map::Entry, HashMap},
    fmt::Debug,
    sync::Arc,
};

use crate::{
    ast::{ConstraintDecl, Csp, Expr, Loc, Metadata, Module, Name, NameId, Named, Parsed},
    Error,
};

pub fn name_ast<'a>(
    mut ast: Module<&'a str, Parsed>,
    errors: &mut Vec<Error>,
) -> (Module<&'a str, Named>, HashMap<usize, &'a str>) {
    let mut state = State {
        csps: HashMap::new(),
        all_variables: HashMap::new(),
        errors,
        fresh_n: 0,
    };
    state.resolve(&mut ast);

    let reverse = state
        .all_variables
        .iter()
        .map(|(k, (_, v))| (*v, *k))
        .collect();

    (ast.change_constraint(), reverse)
}

#[derive(Debug)]
pub enum StoredName<'a, N: AsRef<str>> {
    N(N),
    Ref(&'a N),
    S(&'static str),
}

impl<'a, N: AsRef<str>> From<N> for StoredName<'a, N> {
    fn from(n: N) -> Self {
        Self::N(n)
    }
}

impl<'a, N: AsRef<str>> From<&'a N> for StoredName<'a, N> {
    fn from(n: &'a N) -> Self {
        Self::Ref(n)
    }
}

impl<'a, N: AsRef<str>> AsRef<str> for StoredName<'a, N> {
    fn as_ref(&self) -> &str {
        match self {
            StoredName::N(n) => n.as_ref(),
            StoredName::Ref(n) => n.as_ref(),
            StoredName::S(s) => s,
        }
    }
}

impl<'a, N: AsRef<str>> PartialEq for StoredName<'a, N> {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }
}

impl<'a, N: AsRef<str>> Eq for StoredName<'a, N> {}

impl<'a, N: AsRef<str>> Hash for StoredName<'a, N> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.as_ref().hash(state)
    }
}

struct State<'a, N: AsRef<str>> {
    csps: HashMap<StoredName<'a, N>, (Loc, usize)>,
    all_variables: HashMap<N, (Loc, usize)>,
    errors: &'a mut Vec<Error>,

    fresh_n: usize,
}

impl<'a, N: Hash + Eq + Clone + Debug + AsRef<str>> State<'a, N> {
    fn fresh(&mut self) -> usize {
        let new = self.fresh_n;
        self.fresh_n += 1;
        new
    }

    pub fn get_global(&self, k: impl Into<StoredName<'a, N>>) -> Option<(Loc, usize)> {
        self.csps.get(&k.into()).copied()
    }

    pub fn resolve(&mut self, module: &mut Module<N, Parsed>) {
        self.gather_buildins();
        self.gather_global(module);

        for csp in module.csps.iter_mut() {
            let mut local_state = LocalState {
                global: self,
                locals: HashMap::new(),
            };

            local_state.gather(&mut *csp);
            local_state.resolve(&mut *csp);
        }
    }

    fn gather_buildins(&mut self) {
        let loc = (Loc::Buildin, self.fresh());
        self.csps.insert(StoredName::S("alldiff"), loc);
    }

    fn gather_global(&mut self, module: &mut Module<N, Parsed>) {
        let mut main = None;

        for csp in &mut module.csps {
            match &mut csp.name {
                None => {
                    if let Some(main) = main {
                        self.errors.push(Error::DuplicateName(main, csp.loc));
                    } else {
                        main = Some(csp.loc);
                    }
                }
                Some(name) => match self.get_global(&name.name) {
                    Some((previous_loc, _)) => {
                        self.errors
                            .push(Error::DuplicateName(previous_loc, name.loc));

                        name.name_id = NameId::Error;
                    }
                    None => {
                        let id = self.fresh();
                        self.csps.insert(name.name.clone().into(), (name.loc, id));

                        name.name_id = NameId::Id(id);
                    }
                },
            }
        }
    }
}

struct LocalState<'a, 'b, N: Hash + Eq + AsRef<str>> {
    global: &'a mut State<'b, N>,
    locals: HashMap<N, (Loc, usize)>,
}

impl<'a, 'b, N: Hash + Eq + Clone + Debug + AsRef<str>> LocalState<'a, 'b, N> {
    fn gather_name(&mut self, name: &mut Metadata<Name<N, Parsed>, Parsed>) {
        match self.locals.get(&name.name) {
            Some((previous_loc, _)) => {
                self.global
                    .errors
                    .push(Error::DuplicateName(*previous_loc, name.loc));

                name.name_id = NameId::Error;
            }
            None => {
                let id = self.global.fresh();
                self.locals.insert(name.name.clone(), (name.loc, id));
                self.global
                    .all_variables
                    .insert(name.name.clone(), (name.loc, id));

                name.name_id = NameId::Id(id);
            }
        }
    }

    fn gather(&mut self, csp: &mut Csp<N, Parsed>) {
        for input in &mut csp.input {
            for name in &mut input.names {
                self.gather_name(name)
            }
        }

        for output in &mut csp.output {
            for name in &mut output.names {
                self.gather_name(name)
            }
        }

        for vardecl in &mut csp.body.variables {
            for name in &mut vardecl.names {
                self.gather_name(name)
            }
        }
    }

    fn resolve_name(&mut self, name: &mut Metadata<Name<N, Parsed>, Parsed>) {
        match self.locals.get(&name.name) {
            Some(n) => name.name_id = NameId::Id(n.1),
            None => {
                self.global.errors.push(Error::UnknownName(name.loc));

                name.name_id = NameId::Error;
            }
        }
    }

    fn resolve_name_global(&mut self, name: &mut Metadata<Name<N, Parsed>, Parsed>) {
        match self.global.get_global(&name.name) {
            Some(n) => name.name_id = NameId::Id(n.1),
            None => {
                self.global.errors.push(Error::UnknownName(name.loc));

                name.name_id = NameId::Error;
            }
        }
    }

    fn resolve_expr(&mut self, expr: &mut Expr<N, Parsed>) {
        match expr {
            Expr::Const(_) => {}
            Expr::Var(name) => self.resolve_name(name),
            Expr::BinOp(_, l, r) => {
                self.resolve_expr(&mut *l);
                self.resolve_expr(&mut *r);
            }
            Expr::Call(_, args) => {
                for sub_expr in args {
                    self.resolve_expr(sub_expr);
                }
            }
            Expr::Index(base, args) => {
                self.resolve_expr(base);
                for index in args {
                    for arg in index {
                        self.resolve_expr(arg);
                    }
                }
            }
        }
    }

    fn resolve_constraint(&mut self, constraint: &mut ConstraintDecl<N, Parsed>) {
        match constraint {
            ConstraintDecl::Solve(csp, args) => {
                self.resolve_name_global(csp);
                for arg in args {
                    self.resolve_expr(arg);
                }
            }
            ConstraintDecl::Expr(expr) => self.resolve_expr(expr),
            ConstraintDecl::Forall { name, range, body } => {
                for range in range {
                    // TODO these should not have access to all names
                    self.resolve_expr(range);
                }

                let name_id = self.global.fresh();
                name.name_id = NameId::Id(name_id);
                let previous = self.locals.insert(name.name.clone(), (name.loc, name_id));

                for sub_constraint in body {
                    self.resolve_constraint(sub_constraint);
                }

                if let Some(prev) = previous {
                    self.locals.insert(name.name.clone(), prev);
                } else {
                    self.locals.remove(&name.name);
                }
            }
        }
    }

    fn resolve(&mut self, csp: &mut Csp<N, Parsed>) {
        for domaindecl in &mut csp.body.domains {
            for name in &mut domaindecl.vars {
                self.resolve_name(name);
            }
        }

        for constraintdecl in &mut csp.body.constraints {
            self.resolve_constraint(constraintdecl)
        }
    }
}
