use codespan::{FileId, Span};
use core::hash::Hash;
use core::marker::PhantomData;
use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

#[derive(Debug, Clone, Copy)]
pub enum Loc {
    Loc { file_id: FileId, span: Span },
    Buildin,
}

impl Loc {
    pub fn unwrap(&self) -> (FileId, Span) {
        match self {
            Loc::Loc { file_id, span } => (*file_id, *span),
            Loc::Buildin => panic!("tried to unwrap a buildin loc"),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum NameId {
    Unset,
    Error,
    Id(usize),
}

impl PartialEq for NameId {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (NameId::Id(n1), NameId::Id(n2)) => n1 == n2,
            _ => false,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Name<N, C> {
    pub name: N,
    pub name_id: NameId,
    c: PhantomData<C>,
}
impl<N> Name<N, Parsed> {
    pub fn new(name: N) -> Self {
        Self {
            name,
            name_id: NameId::Unset,
            c: PhantomData::default(),
        }
    }
}

impl<N, C: IsNamed> Name<N, C> {
    pub fn get_id(&self) -> usize {
        if let NameId::Id(n) = self.name_id {
            n
        } else {
            panic!("internal error: {:?}", self.name_id)
        }
    }
}

pub trait IsNamed {}
pub trait IsTyped {}

#[derive(Debug, Clone)]
pub struct Parsed;
#[derive(Debug, Clone)]
pub struct Named;
impl IsNamed for Named {}
#[derive(Debug, Clone)]
pub struct Typed;
impl IsNamed for Typed {}
impl IsTyped for Typed {}

#[derive(Debug, Clone)]
pub struct Metadata<T, C> {
    pub loc: Loc,
    pub inner: T,
    ty: Option<Type>,
    c: PhantomData<C>,
}

impl<T> Metadata<T, Parsed> {
    pub fn new(inner: T, loc: Loc) -> Self {
        Self {
            loc,
            inner,
            ty: None,
            c: PhantomData::default(),
        }
    }
}

impl<T, C> Metadata<T, C> {
    pub fn map<T2>(self, f: impl FnOnce(T) -> T2) -> Metadata<T2, C> {
        Metadata {
            loc: self.loc,
            inner: f(self.inner),
            ty: self.ty,
            c: self.c,
        }
    }
}

impl<T, C: IsTyped> Metadata<T, C> {
    pub fn get_type(&self) -> &Type {
        self.ty.as_ref().unwrap()
    }
}

impl<T, C> Deref for Metadata<T, C> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T, C> DerefMut for Metadata<T, C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

/// A module in the csp (usually a file)
///
/// N is the type of name used, so this can be an owned type/smart pointer or ref.
/// C is the capability of the Module, or the stage at which it is at in the pipeline.
///
/// Futher stages unlock more helper functions to (for example) get the type of something,
/// This one is then only available after type checking.
#[derive(Debug, Clone)]
pub struct Module<N, C> {
    pub file_id: FileId,
    pub csps: Vec<Metadata<Csp<N, C>, C>>,
}

impl<N: Hash + Eq + AsRef<str>, C> Module<N, C> {
    pub fn change_constraint<C2>(self) -> Module<N, C2> {
        unsafe { std::mem::transmute(self) }
    }

    pub fn get_csp(&self, name: Option<usize>) -> Option<&Metadata<Csp<N, C>, C>> {
        self.csps
            .iter()
            .find(|elem| elem.name.as_ref().map(|name| name.name_id) == name.map(|n| NameId::Id(n)))
    }

    pub fn get_main_csp(&self) -> Option<&Metadata<Csp<N, C>, C>> {
        for csp in &self.csps {
            if csp.name.is_none()
                || csp
                    .name
                    .as_ref()
                    .map(|n| n.name.as_ref() == "main")
                    .unwrap_or(false)
            {
                return Some(csp);
            }
        }

        None
    }
}

#[derive(Debug, Clone)]
pub struct Csp<N, C> {
    pub name: Option<Metadata<Name<N, C>, C>>,

    pub input: Vec<Metadata<VariableDecl<N, C>, C>>,
    pub output: Vec<Metadata<VariableDecl<N, C>, C>>,

    pub body: CspBody<N, C>,
}

impl<N, C> Csp<N, C> {
    pub fn try_find_domain(&self, id: usize) -> Option<&Vec<Metadata<RangeDecl<C>, C>>> {
        for domaindecl in &self.body.domains {
            for var in &domaindecl.vars {
                if var.name_id == NameId::Id(id) {
                    return Some(&domaindecl.domain);
                }
            }
        }

        None
    }

    pub fn find_domain(&self, id: usize) -> &Vec<Metadata<RangeDecl<C>, C>> {
        self.try_find_domain(id)
            .expect("internal error, could not find domain. (did the correct pass run?)")
    }
}

#[derive(Debug, Clone)]
pub struct CspBody<N, C> {
    pub variables: Vec<Metadata<VariableDecl<N, C>, C>>,
    pub domains: Vec<Metadata<DomainDecl<N, C>, C>>,
    pub constraints: Vec<Metadata<ConstraintDecl<N, C>, C>>,

    pub solutions: Option<Metadata<Solutions, C>>,
}

#[derive(Debug, Clone)]
pub struct VariableDecl<N, C> {
    pub names: Vec<Metadata<Name<N, C>, C>>,
    pub ty: Metadata<Type, C>,
}

#[derive(Debug, Clone)]
pub struct DomainDecl<N, C> {
    pub vars: Vec<Metadata<Name<N, C>, C>>,
    pub domain: Vec<Metadata<RangeDecl<C>, C>>,
}

#[derive(Debug, Clone)]
pub struct RangeDecl<C> {
    pub start: Metadata<Value, C>,
    pub end: Metadata<Value, C>,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Type {
    Integer,
    Bool,
    Array { subtype: Box<Type>, len: usize },
    Error,
}

impl Type {
    pub fn is_array(&self) -> bool {
        matches!(self, Type::Array { .. })
    }

    pub fn var_count(&self) -> usize {
        match self {
            Type::Array { subtype, len } => subtype.var_count() * len,
            _ => 1,
        }
    }

    pub fn depth(&self) -> usize {
        match self {
            Type::Array { subtype, .. } => 1 + subtype.depth(),
            _ => 0,
        }
    }

    pub fn bottom_type(&self) -> Type {
        match self {
            Type::Array { subtype, .. } => subtype.bottom_type(),
            _ => self.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum ConstraintDecl<N, C> {
    Solve(Metadata<Name<N, C>, C>, Vec<Metadata<Expr<N, C>, C>>),
    Expr(Metadata<Expr<N, C>, C>),
    Forall {
        name: Metadata<Name<N, C>, C>,
        range: Vec<Metadata<Expr<N, C>, C>>,
        body: Vec<Metadata<ConstraintDecl<N, C>, C>>,
    },
}

#[derive(Debug, Clone, Copy)]
pub enum BuildinFunc {
    Abs,
}

#[derive(Debug, Clone)]
pub enum Expr<N, C> {
    Var(Metadata<Name<N, C>, C>),
    Const(Metadata<Value, C>),
    BinOp(
        Metadata<Op, C>,
        Box<Metadata<Expr<N, C>, C>>,
        Box<Metadata<Expr<N, C>, C>>,
    ),
    Index(
        Box<Metadata<Expr<N, C>, C>>,
        Vec<Vec<Metadata<Expr<N, C>, C>>>,
    ),
    Call(Metadata<BuildinFunc, C>, Vec<Metadata<Expr<N, C>, C>>),
}

impl<N, C> Expr<N, C> {
    pub fn is_var(&self) -> bool {
        match self {
            Expr::Var(_) => true,
            _ => false,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum Op {
    EQ,
    NEQ,
    GT,
    LT,
    GE,
    LE,
    Plus,
    Min,
    Times,
    Div,
    Mod,
    BOr,
    Xor,
    Range,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Value {
    Number(isize),
    Bool(bool),
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Solutions {
    All,
    N(usize),
}
