pub enum EitherIter<I1, I2> {
    I1(I1),
    I2(I2),
}

impl<I1, I2, T> Iterator for EitherIter<I1, I2>
where
    I1: Iterator<Item = T>,
    I2: Iterator<Item = T>,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            EitherIter::I1(i) => i.next(),
            EitherIter::I2(i) => i.next(),
        }
    }
}
