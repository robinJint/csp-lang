use std::ops::Range;

use logos::{Logos, Slice, Source};

pub type Spanned<Tok, Loc, Error> = Result<(Loc, Tok, Loc), Error>;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum LexicalError {}

type Lexer<'source> = logos::Lexer<Token, &'source str>;

pub struct LexerIter<'source>(Lexer<'source>, bool, u32);

impl<'source> LexerIter<'source> {
    pub fn new(lxri: &'source str, offset: u32) -> LexerIter<'source> {
        LexerIter(Token::lexer(lxri), true, offset)
    }
}

impl<'source> Iterator for LexerIter<'source> {
    type Item = Spanned<Token, u32, LexicalError>;

    fn next(&mut self) -> Option<Self::Item> {
        if !self.1 {
            self.0.advance();
        }
        self.1 = false;
        if let Token::End = self.0.token {
            return None;
        }
        Some(Ok((
            self.0.range().start as u32 + self.2,
            self.0.token,
            self.0.range().end as u32 + self.2,
        )))
    }
}

fn ignore_comments<'source, Src: Source<'source>>(lex: &mut logos::Lexer<Token, Src>) {
    use logos::internal::LexerInternal;

    if lex.slice().as_bytes() == b"//" {
        loop {
            match lex.read() {
                Some(10) | Some(0) => {
                    break;
                }
                _ => lex.bump(1),
            }
        }
    }

    lex.advance();
}
#[derive(Logos, Debug, PartialEq, Eq, Clone, Copy)]
#[repr(C)]
// #[extras = "LexState"]
pub enum Token {
    // required variants
    #[end]
    End,
    #[regex = "#[^\n]*"]
    #[callback = "ignore_comments"]
    #[error]
    Error,

    // parens
    #[token = "("]
    SParen,
    #[token = ")"]
    EParen,
    #[token = "{"]
    SCurly,
    #[token = "}"]
    ECurly,
    #[token = "["]
    SBlock,
    #[token = "]"]
    EBlock,

    // sigils
    #[token = ","]
    Comma,
    #[token = "."]
    Dot,
    #[token = ".."]
    DoubleDot,
    #[token = ";"]
    SemiColon,
    #[token = "::"]
    DoubleColon,
    #[token = "=>"]
    Arrow,
    #[token = "<-"]
    LeftArrow,
    #[token = "="]
    Is,
    #[token = "!="]
    #[token = "<>"]
    NEQ,
    #[token = "<"]
    LT,
    #[token = ">"]
    GT,
    #[token = "<="]
    LE,
    #[token = ">="]
    GE,
    #[token = "@"]
    At,
    #[token = "*"]
    Times,
    #[token = "-"]
    Min,
    #[token = "+"]
    Plus,
    #[token = "/"]
    #[token = "div"]
    Div,
    #[token = "%"]
    #[token = "mod"]
    Mod,
    #[token = "|"]
    BOr,
    #[token = ":"]
    Colon,

    #[token = "csp"]
    Csp,
    #[token = "begin"]
    Begin,
    #[token = "end"]
    LitEnd,
    #[token = "variables"]
    Variables,
    #[token = "domains"]
    Domains,
    #[token = "constraints"]
    Constraints,
    #[token = "solutions"]
    Solutions,

    #[regex = r"[a-zA-Z_][a-zA-Z0-9_]*"]
    Ident,
    #[token = "all"]
    All,

    #[token = "true"]
    True,
    #[token = "false"]
    False,
    #[token = "integer"]
    Integer,
    #[token = "bool"]
    Bool,
    #[token = "solve"]
    Solve,
    #[token = "input"]
    Input,
    #[token = "output"]
    Output,
    #[token = "forall"]
    Forall,
    #[token = "if"]
    If,
    #[token = "in"]
    In,
    #[token = "abs"]
    Abs,
    #[token = "xor"]
    Xor,

    #[regex = r"[0-9]+"]
    Number,
}
