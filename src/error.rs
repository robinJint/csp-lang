use crate::lexer::{LexicalError, Token};
use crate::{ast::Loc, Error};
use codespan::{FileId, Files, Span};
use codespan_reporting::{
    diagnostic::{Diagnostic, Label, Severity},
    term::{emit, Config},
};
use lalrpop_util::ParseError;
use termcolor::{ColorChoice, StandardStream};

pub fn parse_error_to_diagnostic(
    files: &Files<String>,
    file_id: FileId,
    error: &ParseError<u32, Token, LexicalError>,
) -> Diagnostic {
    match error {
        ParseError::InvalidToken { location } => Diagnostic::new_error(
            "parse error",
            Label::new(file_id, Span::new(*location, *location), "message"),
        ),
        ParseError::UnrecognizedToken {
            token: (l, actual_token, r),
            expected,
        } => {
            let src = files.source_slice(file_id, (*l)..(*r)).unwrap();
            Diagnostic {
                severity: Severity::Error,
                code: Some("E001".into()),
                message: "parse error, unrecognized token".into(),
                primary_label: Label::new(
                    file_id,
                    Span::new(*l, *r),
                    format!(
                        "found: \"{}\", but expected one of [{}]",
                        src,
                        expected.join(", ")
                    ),
                ),
                notes: vec![format!("token was: {:?}", actual_token)],
                secondary_labels: Vec::new(),
            }
        }
        ParseError::UnrecognizedEOF { location, expected } => Diagnostic {
            severity: Severity::Error,
            code: Some("E003".into()),
            message: "parse error, unexpected EOF".into(),
            primary_label: Label::new(
                file_id,
                Span::new(*location, *location),
                format!("end of file, but expected one of [{}]", expected.join(", ")),
            ),
            notes: Vec::new(),
            secondary_labels: Vec::new(),
        },
        ParseError::ExtraToken { token } => panic!("extra token?"),
        ParseError::User { error } => panic!("cannot ever happen"),
    }
}

pub fn error_to_diagnostic(files: &Files<String>, error: &Error) -> Vec<Diagnostic> {
    let mut ds = Vec::new();
    error_to_diagnostic_inner(files, error, &mut ds);
    ds
}

fn error_to_diagnostic_inner(files: &Files<String>, error: &Error, ds: &mut Vec<Diagnostic>) {
    match error {
        Error::Errors(es) => {
            for e in es {
                error_to_diagnostic_inner(files, e, ds);
            }
        }
        Error::ParseError(file_id, parse_error) => {
            ds.push(parse_error_to_diagnostic(&files, *file_id, parse_error));
        }
        Error::Custom(file_id, s) => ds.push(Diagnostic {
            severity: Severity::Error,
            code: Some("E0004".into()),
            message: "internal error".into(),
            primary_label: Label::new(*file_id, Span::new(0, 0), s.to_owned()),
            notes: vec!["Please report this to the issue tracker, with the input file".into()],
            secondary_labels: Vec::new(),
        }),
        Error::DuplicateName(og_name, dub_name) => {
            let mut notes = Vec::new();
            let mut secondary_labels = Vec::new();
            let primary_label = match (og_name, dub_name) {
                (_, Loc::Buildin) => {
                    eprintln!("error in reporting error: duplicate buildin");
                    return;
                }
                (
                    Loc::Loc { file_id, span },
                    Loc::Loc {
                        file_id: dub_file_id,
                        span: dub_span,
                    },
                ) => {
                    secondary_labels.push(Label::new(
                        *dub_file_id,
                        *dub_span,
                        "and again here".to_owned(),
                    ));
                    Label::new(*file_id, *span, "name is already defined here".to_owned())
                }
                (Loc::Buildin, Loc::Loc { file_id, span }) => {
                    notes.push("original name is a buildin name".into());
                    Label::new(*file_id, *span, "and again here".to_owned())
                }
            };
            notes.push("names have to be unique within a csp".into());
            let d = Diagnostic {
                severity: Severity::Error,
                code: Some("E0005".into()),
                message: "dublicate name".into(),
                primary_label,
                notes,
                secondary_labels,
            };
            ds.push(d);
        }
        Error::UnknownName(name) => {
            let (file_id, span) = name.unwrap();
            ds.push(Diagnostic {
                severity: Severity::Error,
                code: Some("E0006".into()),
                message: "no item with this name was found in this scope".into(),
                primary_label: Label::new(file_id, span, "unknown name".to_owned()),
                notes: vec![
                    "maybe the name is misspelled or there is a missing variable declaration"
                        .into(),
                ],
                secondary_labels: Vec::new(),
            })
        }
        Error::IndexOutofbounds {
            loc,
            lower_bound,
            upper_bound,
            index,
        } => {
            let (file_id, span) = loc.unwrap();
            ds.push(Diagnostic {
                severity: Severity::Error,
                code: Some("E0007".into()),
                message: "Index out of bounds".into(),
                primary_label: Label::new(
                    file_id,
                    span,
                    format!(
                        "Valid ranges are {} .. {} (inclusive), but the index was {}",
                        lower_bound, upper_bound, index
                    ),
                ),
                notes: vec![],
                secondary_labels: Vec::new(),
            })
        }
        Error::RangeInNonConst(loc) => {
            let (file_id, span) = loc.unwrap();
            ds.push(Diagnostic {
                severity: Severity::Error,
                code: Some("E0009".into()),
                message: "Range operator in expr".into(),
                primary_label: Label::new(file_id, span, "Range operators cannot be used in this context"),
                notes: vec!["Range operators can only be used in compile time contexts, like domains and as the variable in a forall".into()],
                secondary_labels: Vec::new(),
            })
        }
        Error::MissingDomain(loc) => {
            let (file_id, span) = loc.unwrap();
            ds.push(Diagnostic {
                severity: Severity::Error,
                code: Some("E0008".into()),
                message: "Missing domain".into(),
                primary_label: Label::new(file_id, span, "This variable has no domain"),
                notes: vec!["all variables have to have an assosiated domain".into()],
                secondary_labels: Vec::new(),
            })
        }
        Error::TypeError(l_loc, l_type, r_loc, r_type) => {
            let (l_file_id, l_span) = l_loc.unwrap();
            let (r_file_id, r_span) = r_loc.unwrap();
            ds.push(Diagnostic {
                severity: Severity::Error,
                code: Some("E0010".into()),
                message: "Type error".into(),
                primary_label: Label::new(
                    r_file_id,
                    r_span,
                    format!("Expected {:?} but got {:?}", l_type, r_type),
                ),
                secondary_labels: vec![Label::new(
                    l_file_id,
                    l_span,
                    format!("Expected {:?}, because that is the type used here", l_type),
                )],
                notes: Vec::new(),
            })
        }
        Error::TopLevelTypeError(_, _) => todo!(),
    }
}
