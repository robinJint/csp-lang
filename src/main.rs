#![feature(step_trait, step_trait_ext)]
#![allow(unused_variables, unused_imports, dead_code)]
use std::io::Read;
use std::path::PathBuf;
use std::{fmt::Write, path::Path};
use std::{process::exit, sync::Arc};

use codespan::{FileId, Files, Span};
use codespan_reporting::term::{emit, Config};
use lalrpop_util::lalrpop_mod;
use structopt::StructOpt;
use termcolor::WriteColor;

mod ast;
mod error;
mod lexer;
mod passes;
mod util;
lalrpop_mod!(#[allow(clippy::all)] pub parser);

use ast::Type;
use lexer::{LexicalError, Token};

#[derive(Debug)]
pub enum Error {
    /// Mostly for quick internal errors
    Custom(FileId, &'static str),

    /// A list of other errors
    Errors(Vec<Error>),

    ParseError(FileId, lalrpop_util::ParseError<u32, Token, LexicalError>),
    DuplicateName(ast::Loc, ast::Loc),
    UnknownName(ast::Loc),

    TypeError(ast::Loc, Type, ast::Loc, Type),
    RangeInNonConst(ast::Loc),
    TopLevelTypeError(ast::Loc, Type),

    IndexOutofbounds {
        loc: ast::Loc,
        lower_bound: isize,
        upper_bound: isize,
        index: isize,
    },

    MissingDomain(ast::Loc),
}

#[derive(StructOpt, Debug)]
#[structopt(name = "csp")]
struct Opt {
    /// File to process
    #[structopt(name = "FILE", parse(from_os_str))]
    input: PathBuf,

    /// Only check syntax/types, don't run anything
    #[structopt(long = "check")]
    check: bool,

    #[structopt(short = "v", long = "debug")]
    debug: bool,

    #[structopt(short = "s", long = "hide-stats", multiple = true)]
    hide_stats: bool,

    /// use the Minimum remaining values (MRV) to select the next variable
    #[structopt(long = "mrv")]
    mrv: bool,

    /// Make the csp node consistent before solving
    #[structopt(long = "nc")]
    node_consistent: bool,

    /// Make the csp (generalized) arc consistent before solving
    #[structopt(long = "ac", multiple = true)]
    arc_consistent: bool,

    /// Use Forward checking
    #[structopt(long = "fc", multiple = true)]
    forward_checking: bool,
    /// Maintain arc concitency (MAC)
    #[structopt(long = "mac", multiple = true)]
    mac: bool,
}

fn load_file(path: &Path) -> String {
    let mut buf = String::new();
    std::fs::File::open(path)
        .unwrap()
        .read_to_string(&mut buf)
        .unwrap();

    buf
}

pub fn parse_ast(src: &str, file_id: FileId) -> Result<ast::Module<&str, ast::Parsed>, Error> {
    let token_iter: Box<dyn Iterator<Item = crate::lexer::Spanned<Token, u32, LexicalError>>> =
        Box::new(crate::lexer::LexerIter::new(&src, 0));

    let ast = crate::parser::ModuleParser::new().parse(&src, file_id, token_iter);

    ast.map_err(|e| Error::ParseError(file_id, e))
}

fn run(opt: &Opt, files: &mut Files<String>) -> Result<(), Error> {
    let opt = Opt::from_args();
    if opt.debug {
        env_logger::init()
    }
    let contents = load_file(&opt.input);

    let main_file_id = files.add("main", contents);

    let mut errors = Vec::new();

    let ast = parse_ast(files.source(main_file_id), main_file_id)?;

    let (named_ast, name_mapping) = passes::naming::name_ast(ast, &mut errors);

    if !errors.is_empty() {
        return Err(Error::Errors(errors));
    }

    let typed_ast = passes::typechecking::typecheck(named_ast, &mut errors);

    if !errors.is_empty() {
        return Err(Error::Errors(errors));
    }

    let lowered = match passes::lowering::lower(&typed_ast) {
        Ok(l) => l,
        Err(es) => {
            errors.push(es);
            return Err(Error::Errors(errors));
        }
    };

    if !errors.is_empty() {
        return Err(Error::Errors(errors));
    }

    let (mut csp, compile_mapping) = passes::compiling::compile(&lowered)?;

    if opt.arc_consistent {
        csp.ac2();
    } else if opt.node_consistent {
        csp.node_consistent();
    }
    csp.forward_checking = opt.forward_checking;
    csp.mac = opt.mac;
    csp.next_variable = csp_lib_rs::NextVariable::Mrv;

    if opt.check {
        println!("check parameter passed, not running the csp");
        return Ok(());
    }

    let result = csp_lib_rs::solve(&csp);

    if result.solutions.is_empty() {
        println!("no solutions");
    }

    for (n, solution) in result.solutions.iter().enumerate() {
        println!("solution {}:", n);
        for (i, value) in solution.iter().enumerate() {
            let name: String = compile_mapping
                .get(&i)
                .and_then(|index| lowered.lookup.get(index))
                .and_then(|(var, index)| {
                    let mut name: String = name_mapping.get(var)?.to_owned().to_owned();
                    for index in index {
                        write!(&mut name, "[{}]", index).unwrap();
                    }
                    Some(name)
                })
                .unwrap_or_else(|| "?".to_owned());
            println!("  {:<15} = {}", name, value);
        }
    }

    if !opt.hide_stats {
        println!(
            "visited states: {}/{}",
            result.visited_states, result.all_possible_states
        );
    }

    Ok(())
}

fn report_error(
    writer: &mut impl WriteColor,
    config: &Config,
    files: &Files<String>,
    error: &Error,
) {
    let ds = error::error_to_diagnostic(files, error);
    ds.into_iter()
        .for_each(|d| emit(writer, &config, &files, &d).unwrap());
}

fn main() {
    color_backtrace::install();
    env_logger::init();

    let opt = Opt::from_args();

    let mut files = Files::new();
    let config = Config::default();
    let mut writer = termcolor::BufferedStandardStream::stderr(termcolor::ColorChoice::Auto);

    match run(&opt, &mut files) {
        Ok(()) => {}
        Err(error) => {
            report_error(&mut writer, &config, &files, &error);
        }
    };
}
