use std::str::FromStr;
use core::marker::PhantomData;

use lalrpop_util::ErrorRecovery;
use codespan::{Span, FileId};

use crate::lexer::{self, Token, LexicalError};
use crate::ast::*;

grammar<'input>(filemap: &'input str, file_id: FileId);


pub Module: Module<&'input str, Parsed> = Metadata<TopLevel>* => Module {csps: <>, file_id};


TopLevel: Csp<&'input str, Parsed> = {
    "begin" <Csp> "end" => Csp {
        body: <>,
        name: None,
        input: Vec::new(),
        output: Vec::new(),
    },
    "csp" <name: Metadata<Name>> "(" 
        <input: ("input" ":" <(<Metadata<VariableDecl>> ";")+>)?>
        <output: ("output" ":" <(<Metadata<VariableDecl>> ";")+>)?>
    ")" "begin"
        <csp: Csp> 
    "end" => {
        Csp {
            name: Some(name),
            input: input.unwrap_or_default(),
            output: output.unwrap_or_default(),

            body: csp,
        }
    },
};

Csp: CspBody<&'input str, Parsed> = 
    <variables: ("variables" ":" <(<Metadata<VariableDecl>> ";")+>)?>
    <domains: ("domains" ":" <(<Metadata<DomainDecl>> ";")+>)?>
    <constraints: ("constraints" ":" <Metadata<ConstraintDecl>+>)?>
    <solutions: ("solutions" ":" <Metadata<Solutions>> ";")?>
    => CspBody {
        variables: variables.unwrap_or_default(),
        domains: domains.unwrap_or_default(),
        constraints: constraints.unwrap_or_default(),
        solutions: solutions,
    };

VariableDecl: VariableDecl<&'input str, Parsed> = <names: SepBy<Metadata<Name>, ",">> ":" <ty: Metadata<Type>> =>
    VariableDecl{names, ty};

DomainDecl: DomainDecl<&'input str, Parsed> = <vars: SepBy<Metadata<Name>, ",">> "<-" "[" <domain: SepBy<Metadata<Range>, ",">> "]" =>
    DomainDecl {vars, domain};

ForallRange: Vec<Metadata<Expr<&'input str, Parsed>, Parsed>> = "[" <SepBy<Metadata<Expr>, ",">> "]";

ConstraintDecl: ConstraintDecl<&'input str, Parsed> = {
    "solve" <name: Metadata<Name>> "(" <args: SepBy<Metadata<Expr>, ",">> ")" ";" => ConstraintDecl::Solve(name, args),
    "forall" "(" <name: Metadata<Name>> "in" <range: ForallRange> ")" <body: Metadata<ConstraintDecl>*> "end" =>
        ConstraintDecl::Forall {name, range, body},
    <Metadata<Expr>> ";" => ConstraintDecl::Expr(<>),
};

Expr: Expr<&'input str, Parsed> = {
    Expr2 => <>,
    <l: Metadata<Expr>> <op: Metadata<Op1>> <r: Metadata<Expr2>> => Expr::BinOp(op, l.into(), r.into()),
};

Op1: Op = {
    "=" => Op::EQ,
    "!=" => Op::NEQ,
    "<" => Op::LT,
    ">" => Op::GT,
    "<=" => Op::LE,
    ">=" => Op::GE,
}

Expr2: Expr<&'input str, Parsed> = {
    Expr3 => <>,
    <l: Metadata<Expr2>> <op: Metadata<Op2>> <r: Metadata<Expr3>> => Expr::BinOp(op, l.into(), r.into()),
};

Op2: Op = {
    ".." => Op::Range,
}

Expr3: Expr<&'input str, Parsed> = {
    Expr4 => <>,
    <l: Metadata<Expr3>> <op: Metadata<Op3>> <r: Metadata<Expr4>> => Expr::BinOp(op, l.into(), r.into()),
};


Op3: Op = {
    "+" => Op::Plus,
    "-" => Op::Min,
    "|" => Op::BOr,
    "xor" => Op::Xor,
}

Expr4: Expr<&'input str, Parsed> = {
    BaseExpr => <>,
    <l: Metadata<Expr4>> <op: Metadata<Op4>> <r: Metadata<BaseExpr>> => Expr::BinOp(op, l.into(), r.into()),
};

Op4: Op = {
    "*" => Op::Times,
    "/" => Op::Div,
    "%" => Op::Mod,
}

BaseExpr: Expr<&'input str, Parsed> = {
    Metadata<Name> => Expr::Var(<>),
    Metadata<Value> => Expr::Const(<>),
    "(" <Expr> ")" => <>,
    <base: Metadata<BaseExpr>> "[" <index: SepByStrict<SepBy<Metadata<Expr>, ",">, ";">> "]" => 
        Expr::Index(Box::new(base), index),
    <f: Metadata<"abs">> "(" <args: SepBy<Metadata<Expr>, ",">> ")" => Expr::Call(f.map(|_| BuildinFunc::Abs), args),
}

Solutions: Solutions = {
    "all" => Solutions::All,
    UNumber => Solutions::N(<>)
}

Type: Type = {
    "integer" => Type::Integer,
    "bool" => Type::Bool,
    "[" <ty: Type> ";" <len: UNumber> "]" =>
        Type::Array {subtype: Box::new(ty), len}
}

Range: RangeDecl<Parsed> = <start: Metadata<Value>> ".." <end: Metadata<Value>> => RangeDecl {start, end};

Value: Value = {
    Number => Value::Number(<>),
    "true" => Value::Bool(true),
    "false" => Value::Bool(false),
};

Name: Name<&'input str, Parsed> = Ident => Name::new(<>);

Ident: &'input str = <l:@L> ident <r:@R> => {
    &filemap[l as usize .. r as usize]
};

UNumber: usize = <l:@L> number <r:@R> => {
    let text = &filemap[l as usize .. r as usize];
    usize::from_str(text).unwrap()
};

Number: isize = <min: "-"?> <num: UNumber> => {
    let num = num as isize;
    if min.is_some() {
        -num
    } else {
        num
    }
};

Metadata<T>: Metadata<T, Parsed> = <l:@L> <t: T> <r:@R> => {
    Metadata::new(t, Loc::Loc{file_id, span: Span::new(l, r)})
};

SepByStrict<T, S>: Vec<T> = {
    <v:(<T> S)*> <e:T> => {
        let mut v = v;
        v.push(e);
        v
    }
};

SepBy<T, S>: Vec<T> = {
    <v:(<T> S)*> <e:T?> => match e {
        None => v,
        Some(e) => {
            let mut v = v;
            v.push(e);
            v
        }
    }
};

extern {
    type Location = u32;
    type Error = lexer::LexicalError;

    enum lexer::Token {
        "(" => lexer::Token::SParen,
        ")" => lexer::Token::EParen,
        "{" => lexer::Token::SCurly,
        "}" => lexer::Token::ECurly,
        "[" => lexer::Token::SBlock,
        "]" => lexer::Token::EBlock,

        "," => lexer::Token::Comma,
        "." => lexer::Token::Dot,
        ".." => lexer::Token::DoubleDot,
        ";" => lexer::Token::SemiColon,
        ":" => lexer::Token::Colon,
        "=" => lexer::Token::Is,
        "!=" => lexer::Token::NEQ,
        ">" => lexer::Token::GT,
        "<" => lexer::Token::LT,
        ">=" => lexer::Token::GE,
        "<=" => lexer::Token::LE,
        "=>" => lexer::Token::Arrow,
        "<-" => lexer::Token::LeftArrow,
        "*" => lexer::Token::Times,
        "-" => lexer::Token::Min,
        "+" => lexer::Token::Plus,
        "/" => lexer::Token::Div,
        "%" => lexer::Token::Mod,
        "|" => lexer::Token::BOr,

        "begin" => lexer::Token::Begin,
        "end" => lexer::Token::LitEnd,
        "csp" => lexer::Token::Csp,
        "variables" => lexer::Token::Variables,
        "domains" => lexer::Token::Domains,
        "constraints" => lexer::Token::Constraints,
        "solutions" => lexer::Token::Solutions,
        "all" => lexer::Token::All,
        "true" => lexer::Token::True,
        "false" => lexer::Token::False,
        "integer" => lexer::Token::Integer,
        "bool" => lexer::Token::Bool,
        "solve" => lexer::Token::Solve,
        "input" => lexer::Token::Input,
        "output" => lexer::Token::Output,
        "forall" => lexer::Token::Forall,
        "if" => lexer::Token::If,
        "in" => lexer::Token::In,
        "abs" => lexer::Token::Abs,
        "xor" => lexer::Token::Xor,

        number => lexer::Token::Number,
        ident => lexer::Token::Ident,
    }
}